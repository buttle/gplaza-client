import { createRouter, createWebHashHistory } from "vue-router";
import NecesidadesView from "../views/NecesidadesView.vue";
import DonesView from "../views/DonesView.vue";
import FaqView from "../views/FaqView.vue";

export const router = createRouter({
  // Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      name: "home",
      redirect: "/Donosti/necesidades",
    },
    {
      path: "/:community/necesidades",
      name: "necesidades",
      component: NecesidadesView,
    },
    {
      path: "/:community/dones",
      name: "dones",
      component: DonesView,
    },
    {
      path: "/faq",
      name: "faq",
      component: FaqView,
    },
  ],
});
